/* MultiMower
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Dry/Audio/SoundListener.h"
#include "inputmaster.h"
#include "player.h"

#include "jib.h"


Jib::Jib(Context* context): LogicComponent(context),
    cameraNode_{ nullptr },
    target_{ nullptr },
    camera_{ nullptr }
{
}

void Jib::OnNodeSet(Node* node)
{
    node_->SetPosition(Vector3::UP * 5.f);
    cameraNode_ = node_->CreateChild("Camera");

    cameraNode_->SetPosition(Vector3{ 0.f, 2.f, -5.f });
    cameraNode_->LookAt(node_->GetWorldPosition());
    camera_ = cameraNode_->CreateComponent<Camera>();
    camera_->SetFov(95.f);

    SharedPtr<Viewport> viewport{ new Viewport{ context_, GetScene(), camera_ } };

//    SharedPtr<RenderPath> effectRenderPath{ viewport->GetRenderPath()->Clone() };
//    effectRenderPath->Append(CACHE->GetResource<XMLFile>("PostProcess/FXAA3.xml"));
//    effectRenderPath->SetEnabled("FXAA3", true);
//    viewport->SetRenderPath(effectRenderPath);

    RENDERER->SetViewport(0, viewport);

    AUDIO->SetListener(cameraNode_->CreateComponent<SoundListener>());

    Rotate({ 0.f, -GRAPHICS->GetHeight() / 9.f });
}

void Jib::Update(float timeStep)
{
    // Spectator only?
    const int wheel{ INPUT->GetMouseMoveWheel() };
//    cameraNode_->Translate(cameraNode_->GetDirection() * wheel * 2.f);
//    camera_->SetFov(Clamp(camera_->GetFov() - wheel * 2.3f, 55.f, 111.f));

    if (target_)
    {
        const Vector3 pos{ target_->GetWorldPosition() + Vector3::UP * 3.0f };
        node_->SetWorldPosition(pos);
    }

    Vector2 mouseMove{ INPUT->GetMouseMove() * 23.f };

    Rotate(mouseMove * timeStep);


}

void Jib::SetPlayer(Player* player)
{
    Controllable* controllable{ GetSubsystem<InputMaster>()->GetControllableByPlayer(player->GetId()) };

    if (controllable)
        SetTarget(controllable->GetNode());
}

void Jib::Rotate(const Vector2 rotation)
{
    node_->Yaw(rotation.x_);
    cameraNode_->RotateAround(cameraNode_->WorldToLocal(node_->GetWorldPosition() - node_->GetWorldDirection() * 2.f), Quaternion{ .5f * rotation.y_, Vector3::RIGHT }, TS_LOCAL);
}
