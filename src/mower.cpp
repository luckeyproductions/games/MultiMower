/* MultiMower
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Dry/Physics/RaycastVehicle.h"
#include "player.h"
#include "jib.h"

#include "mower.h"

Mower::Mower(Context* context): Controllable(context),
    vehicle_{ nullptr },
    gun_{},
    bullets_{},
    sparkles_{},
    trailers_{},
    missiles_{{},{}},
    bulletInterval{ .08f },
    sinceBullet{ bulletInterval },
    missileInterval{ .125f },
    sinceMissile{ missileInterval },
    lastFiredRight_{ false }
{
}

void Mower::OnSetEnabled() { LogicComponent::OnSetEnabled(); }
void Mower::Start() {}
void Mower::DelayedStart() {}
void Mower::Stop() {}
void Mower::Update(float timeStep)
{
    if (sinceBullet < bulletInterval)
        sinceBullet += timeStep;
    if (sinceMissile < missileInterval)
        sinceMissile += timeStep;

    if (GetPlayer())
    {
        HandleInput();
    }
    else
    {
        if (Random(420) == 0)
            FireMissile();
    }

    for (unsigned b{ 0u }; b < bullets_.Size(); ++b)
    {
        Projectile& bullet{ bullets_.At(b)};

        bullet.age_ += timeStep;
        if (bullet.age_ > 0.17f || bullet.path_.Solve(bullet.age_).y_ < .075f)
        {
            for (int s{ 0 }; s < 23; ++s) ///
                Sparkle(bullet.path_.Solve(bullet.age_), 5.f);
            bullets_.EraseSwap(b);
        }
    }

    for (unsigned s{ 0u }; s < sparkles_.Size(); ++s) ///
    {
        Projectile& sparkle{ sparkles_.At(s)};

        sparkle.age_ += timeStep;
        if (sparkle.age_ > 0.17f)
        {
            sparkles_.EraseSwap(s);
        }
    }

    for (bool left: { true, false })
    {
        Projectile& missile{ (left ? missiles_.first_ : missiles_.second_) };

        if (!missile.path_.IsConstant())
        {
            missile.age_ += timeStep;
            Sparkle(missile.path_.Solve(missile.age_), .1f);

            if (missile.age_ > 4.f/3.f || missile.path_.Solve(missile.age_).y_ < .25f)
            {
                const int trailCount{ 23 * (1 + (Random(4))) };

                for (int s{ 0 }; s < trailCount; ++s) ///
                {
                    Trail(missile.path_.Solve(missile.age_));
                }

                Node* lightNode{ GetScene()->CreateChild("Light") };
                lightNode->CreateComponent<BlastLight>();
                lightNode->SetPosition(missile.path_.Solve(missile.age_) * Vector3{ 1.f, .5f, 1.f } + Vector3::UP * 3.f);
                Color trailCol{};

                trailCol.FromHSV(GetScene()->GetElapsedTime() * 999.f, 1.f, 1.f);
                lightNode->GetComponent<Light>()->SetColor(trailCol.Lerp({ 1.f, .95f, .9f }, 1.f/4.f));

                missile.path_ = {};
                missile.age_ = 0.f;

                PlaySample(RES(Sound, "Samples/Explode.wav"), 2.f/3.f);
            }
        }
    }

    for (unsigned s{ 0u }; s < trailers_.Size(); ++s) ///
    {
        Projectile& trailer{ trailers_.At(s)};
        Sparkle(trailer.path_.Solve(trailer.age_));

        trailer.age_ += timeStep;
        if (trailer.age_ > 0.9f)
        {
            trailers_.EraseSwap(s);
        }
    }
}

void Mower::HandleInput()
{
    // Fire bullet
    if (INPUT->GetMouseButtonDown(MOUSEB_LEFT) && sinceBullet >= bulletInterval)
    {
        bullets_.Push(Projectile{ gun_ });
        sinceBullet = 0.f;
    }

    // Fire missile
    if (INPUT->GetMouseButtonPress(MOUSEB_RIGHT) && sinceMissile >= missileInterval)
    {
        FireMissile();
    }
}

void Mower::FireMissile()
{
    Projectile& missile{ (lastFiredRight_ ? missiles_.first_ : missiles_.second_) };

    if (!missile.path_.IsConstant())
        return;

    Node* missileNode{ node_->GetChild((lastFiredRight_ ? "Missile.L" : "Missile.R"), true) };

    Player* player{ GetPlayer() };
    Jib* jib{ player ? GetPlayer()->GetJib() : nullptr };

    const Vector3 sway{ (jib ? jib->GetLookDirection().ProjectOntoPlane(missileNode->GetWorldUp())
                             : Vector3{ RandomOffCenter(.5f), Random(.5f, 2.f), RandomOffCenter(.5f) }.Normalized()) };
    const float away{ Sqrt(sway.Length()) };

    missile.path_ = {{ missileNode->GetWorldPosition(),
                       Vector3::ZERO,
                       Vector3::UP * 23.f + missileNode->GetWorldUp() * 50.f,
                       Vector3::DOWN * 42.f + sway * 99.f * away }};
    missile.age_ = 0.f;
    sinceMissile = 0.f;

    lastFiredRight_ = !lastFiredRight_;

    PlaySample(RES(Sound, "Samples/Launch.wav"), .07f);
}

void Mower::PostUpdate(float timeStep)
{
    if (!GetPlayer())
        return;

    Jib* jib{ GetPlayer()->GetJib() };

    if (!jib)
        return;

    Node* barrelNode{ node_->GetChild("Barrel", true) };
    Node* lowerArm{   node_->GetChild("Arm1.M", true) };
    Node* upperArm{   node_->GetChild("Arm2.M", true) };
    Node* yawNode{    node_->GetChild("Arm3.M", true) };

    if (barrelNode && yawNode && lowerArm && upperArm)
    {
        const Vector3 up{ yawNode->GetWorldUp() };
        const Vector3 barrelDirection{ barrelNode->GetWorldUp() };
        const Vector3 flatBarrel{ barrelDirection.ProjectOntoPlane(up).Normalized() };
        const Vector3 lookDirection{ jib->GetLookDirection() };
        const Vector3 flatLook{ lookDirection.ProjectOntoPlane(barrelNode->GetWorldDirection()).Normalized() };

        const Vector3 yawCross{ flatBarrel.CrossProduct(flatLook) };
        const Vector3 pitchCross{ flatLook.CrossProduct(lookDirection + Vector3::DOWN * .2f) };

        const float away{ 2.3f + barrelDirection.Angle(lookDirection) / 90.f };

        const float pitch{ Asin(pitchCross.DotProduct(jib->GetRight())) * Min(1.f, timeStep * 3.f * away ) };
        const float yaw{   Asin(yawCross.DotProduct(up))                * Min(1.f, timeStep * 4.f * away * away ) };

        yawNode->Yaw(yaw);
        barrelNode->Pitch(pitch);

        Vector3 stray{ Vector3{ RandomOffCenter(1.f), RandomOffCenter(1.f), RandomOffCenter(1.f) }.ProjectOntoPlane(barrelDirection) };

        gun_ = {{ barrelNode->GetWorldPosition() + barrelDirection, barrelDirection * 420.f, Vector3::DOWN
                + 42.f * stray, -5.f * stray }};
    }

    for (bool left: { true, false })
    {
        Projectile& missile{ (left ? missiles_.first_ : missiles_.second_) };
        Node* missileNode{ node_->GetChild((left ? "Missile.L" : "Missile.R"), true) };

        if (!missile.path_.IsConstant())
        {
            missileNode->SetWorldPosition(missile.path_.Solve(missile.age_));
            Quaternion rot{};
            rot.FromLookRotation(missile.path_.Derived().Solve(missile.age_));
            missileNode->LookAt(missileNode->GetWorldPosition() + rot * Vector3::UP);
        }

        else if (missileNode->GetPosition() != Vector3::ZERO)
        {
            missileNode->SetPosition(Vector3::ZERO);
            missileNode->SetRotation(Quaternion{ 0.f, 0.f, left ? -90.f : 90.f });
        }
    }
}
void Mower::FixedUpdate(float timeStep) {}
void Mower::FixedPostUpdate(float timeStep) {}
void Mower::OnNodeSet(Node* node)
{
    AnimatedModel* mowerModel{ node_->CreateComponent<AnimatedModel>() };
    mowerModel->SetModel(RES(Model, "Models/Mower.mdl"));
    mowerModel->SetMaterial(RES(Material, "Materials/VColHighSpec.xml"));
    mowerModel->SetCastShadows(true);

    RigidBody* mowerBody{ node_->CreateComponent<RigidBody>() };
    mowerBody->SetMass(8.f);
    CollisionShape* collider{ node_->CreateComponent<CollisionShape>() };
    collider->SetBox({ 2.f, .75f, 1.9f }, { 0.f, .4f, .2f });

    vehicle_ = node_->CreateComponent<RaycastVehicle>();
    vehicle_->Init();

    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Mower, RenderDebug));
}

void Mower::RenderDebug(StringHash eventType, VariantMap& eventData)
{
    DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };

    for (const Projectile& b: bullets_)
    {
        const float t{ 2.f * b.age_ };

        debug->AddLine(b.path_.Solve(Max(0.f, b.age_ - .05f)), b.path_.Solve(b.age_), Color::WHITE.Lerp(Color::YELLOW, t + .2f).Transparent(1.f - t));
    }
    for (const Projectile& s: sparkles_)
    {
        const float t{ 3.f * s.age_ };
        debug->AddLine(s.path_.Solve(Max(0.f, s.age_ - .05f)), s.path_.Solve(s.age_), Color::WHITE.Lerp(Color::YELLOW.Lerp(Color::ORANGE, t * 1.75f), 1.5f * t + .1f).Transparent(Pow(Max(0.f, 1.f - t), 1.5f)));
    }
    for (const Trailer& r: trailers_)
    {
        const Vector3 pos{ r.path_.Solve(r.age_) };
        const float size{ (2.f - r.age_) };

        debug->AddCross(pos, size, r.color_.Lerp(Color{r.color_ - Color{.5f, .25f, 1.f}}, r.age_).Transparent(Sqrt(Max(0.f, .125f + .5f * size))));
    }
//    Camera* camera{ GetPlayer()->GetJib()->GetCamera() };
//    const Vector2 screenPos{ .5f, .6f + .55f * (Max(0.f, camera->GetScreenRay({ .5f, .5f }).direction_.y_ - .17f)) };
//    const Vector3 hitPos{ camera->ScreenToPlanePos(screenPos, { Vector3::UP, Vector3::ZERO })};

//    debug->AddCross(hitPos, 2.f, Color::RED, false);
}

void Mower::OnSceneSet(Scene* scene) { LogicComponent::OnSceneSet(scene); }
void Mower::OnMarkedDirty(Node* node) {}
void Mower::OnNodeSetEnabled(Node* node) {}
bool Mower::Save(Serializer& dest) const { return LogicComponent::Save(dest); }
bool Mower::SaveXML(XMLElement& dest) const { return LogicComponent::SaveXML(dest); }
bool Mower::SaveJSON(JSONValue& dest) const { return LogicComponent::SaveJSON(dest); }
void Mower::MarkNetworkUpdate() { LogicComponent::MarkNetworkUpdate(); }
void Mower::GetDependencyNodes(PODVector<Node*>& dest) {}
void Mower::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}

void Mower::Sparkle(const Vector3& position, float speed)
{
    sparkles_.Push(Projectile{ {{ position, Vector3{ RandomOffCenter(1.f), RandomOffCenter(1.f), RandomOffCenter(1.f) } * 17.f * speed, Vector3::DOWN * 23.f }} });
}
void Mower::Trail(const Vector3& position)
{
    const Vector3 direction{ Vector3{ RandomOffCenter(1.f), RandomOffCenter(1.f), RandomOffCenter(1.f) } };
    trailers_.Push(Trailer{ {{ position, direction * 90.f, Vector3::DOWN * 17.f - direction * 40.f }} });
    Color trailCol{};

    trailCol.FromHSV(GetScene()->GetElapsedTime() * 999.f, 1.f, 1.f);

    trailers_.Back().color_ = trailCol;
}
