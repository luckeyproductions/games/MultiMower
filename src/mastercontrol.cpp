/* MultiMower
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "inputmaster.h"
#include "player.h"
#include "mower.h"
#include "jib.h"
#include "spawnmaster.h"

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context *context): Application(context),
    players_{}
{
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "MultiMower.log";
    engineParameters_[EP_WINDOW_TITLE] = "MultiMower";
    engineParameters_[EP_WINDOW_ICON]  = "icon.png";
    engineParameters_[EP_WORKER_THREADS] = false;
    engineParameters_[EP_RESOURCE_PATHS] = "Resources;";

//    engineParameters_[EP_FULL_SCREEN] = false;
//    engineParameters_[EP_WINDOW_WIDTH] = 1600;
//    engineParameters_[EP_WINDOW_HEIGHT] = 900;
//    engineParameters_[EP_BORDERLESS] = true;

}

void MasterControl::Start()
{
    context_->RegisterSubsystem(this);
    context_->RegisterSubsystem<InputMaster>();
    context_->RegisterSubsystem<SpawnMaster>();

    context_->RegisterFactory<Mower>();
    context_->RegisterFactory<Player>();
    context_->RegisterFactory<Jib>();
    context_->RegisterFactory<BlastLight>();

    if (GRAPHICS)
        ENGINE->SetMaxFps(GRAPHICS->GetRefreshRate());

    CreateScene();
}

void MasterControl::Stop()
{
    engine_->DumpResources(true);
}

void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::CreateScene()
{
    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();
    scene_->CreateComponent<PhysicsWorld>();
    scene_->CreateComponent<DebugRenderer>()->SetLineAntiAlias(true);

    //Light
    Node* lightNode{ scene_->CreateChild("Light") };
    lightNode->SetPosition(Vector3(2.0f, 3.0f, 1.0f));
    lightNode->LookAt(Vector3::ZERO);
    Light* sun{ lightNode->CreateComponent<Light>() };
    sun->SetLightType(LIGHT_DIRECTIONAL);
//    sun->SetCastShadows(true);
    sun->SetBrightness(0.2f);
    sun->SetColor({ 0.7f, 0.9f, 1.f });

    //Camera
    Node* cameraNode{ scene_->CreateChild("Camera") };
    cameraNode->SetPosition(Vector3::ONE * 6.0f);
    cameraNode->LookAt(Vector3::UP * 1.f);
    Camera* camera{ cameraNode->CreateComponent<Camera>() };
    camera->SetFov(80.f);
    RENDERER->SetViewport(0, new Viewport(context_, scene_, camera));
    //Walls
    Node* wallNode{ scene_->CreateChild() };
    wallNode->SetScale(Vector3{ 42.f, 1.f, 55.f} * 42.f);
    StaticModel* wallModel{ wallNode->CreateComponent<StaticModel>() };
    wallModel->SetModel(CACHE->GetResource<Model>("Models/Plane.mdl"));
    wallModel->SetMaterial(RES(Material, "Materials/MudLeavesTiled.xml"));

    RigidBody* floorBody{ wallNode->CreateComponent<RigidBody>() };
    CollisionShape* floorCollider{ wallNode->CreateComponent<CollisionShape>() };
    floorCollider->SetBox(Vector3::ONE, Vector3::DOWN * .5f);


    for (int x{ -1 }; x <= 1; ++x)
    for (int z{  0 }; z <= 1; ++z)
    {
        Mower* mower{ SPAWN->Create<Mower>() };
        mower->Set({ x * 50.f, 0.f, z * 50.f });

        if (x == 0 && z == 0)
        {
            GetSubsystem<InputMaster>()->SetPlayerControl(AddPlayer(), mower);
            GetPlayers().Back()->SetJib(jibs_.Back());
        }
    }
}


Player* MasterControl::AddPlayer()
{
    players_.Push(context_->CreateObject<Player>());
    jibs_.Push(scene_->CreateChild("Jib")->CreateComponent<Jib>());
    return players_.Back();
}

Vector<SharedPtr<Player> > MasterControl::GetPlayers()
{
    return players_;
}

void MasterControl::RemovePlayer(Player* player)
{
    for (unsigned p{ 0 }; p < players_.Size(); ++p)
    {
        if (players_.At(p)->GetId() == player->GetId())
        {
            jibs_.At(p)->GetNode()->Remove();
            jibs_.EraseSwap(p);
            players_.EraseSwap(p);

            return;
        }
    }
}

Player* MasterControl::GetPlayer(int playerId) const
{
    for (Player* p: players_)
    {
        if (p->GetId() == playerId)
        {
            return p;
        }
    }
    return nullptr;
}

Player* MasterControl::GetNearestPlayer(Vector3 pos)
{
    Player* nearest{};
    for (Player* p : players_)
    {
        if (p->IsAlive())
        {
            if (!nearest
                || (LucKey::Distance(GetSubsystem<InputMaster>()->GetControllableByPlayer(p->GetId())->GetPosition(), pos) <
                    LucKey::Distance(GetSubsystem<InputMaster>()->GetControllableByPlayer(nearest->GetId())->GetPosition(), pos)))
            {
                nearest = p;
            }
        }
    }

    return nearest;
}
