HEADERS += \
    $$PWD/luckey.h \
    $$PWD/mastercontrol.h \
    $$PWD/mower.h \
    $$PWD/sceneobject.h \
    $$PWD/spawnmaster.h \
    $$PWD/inputmaster.h \
    $$PWD/player.h \
    $$PWD/jib.h \
    $$PWD/controllable.h \

SOURCES += \
    $$PWD/luckey.cpp \
    $$PWD/mastercontrol.cpp \
    $$PWD/mower.cpp \
    $$PWD/sceneobject.cpp \
    $$PWD/spawnmaster.cpp \
    $$PWD/inputmaster.cpp \
    $$PWD/player.cpp \
    $$PWD/jib.cpp \
    $$PWD/controllable.cpp \
