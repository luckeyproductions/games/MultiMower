/* MultiMower
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MOWER_H
#define MOWER_H

#include "controllable.h"

class BlastLight: public LogicComponent
{
    DRY_OBJECT(BlastLight, LogicComponent);

public:
    BlastLight(Context* context): LogicComponent(context),
        light_{ nullptr }
    {
    }

    void Update(float timeStep) override
    {
        if (!light_)
            return;

        light_->SetBrightness(Max(0.f, light_->GetBrightness() - timeStep * 16.f));
        light_->SetColor(light_->GetColor().Lerp({ 1.f, .95f, .9f }, Min(1.f, timeStep * 2.3f)));

        if (light_->GetBrightness() <= 0.f)
        {
            node_->Remove();
        }
    }

protected:
    void OnNodeSet(Node* node) override
    {
        if (!node)
            return;

        light_ = node_->CreateComponent<Light>();
        light_->SetColor({ 1.f, .95f, .9f });
        light_->SetRange(128.f);;
        light_->SetBrightness(8.f);
//        light_->SetCastShadows(true);
    }

    Light* light_;
};

struct Projectile
{
    Projectile() = default;

    explicit Projectile(const TypedPolynomial<Vector3>& path):
        path_{ path },
        age_{ 0.f }
    {
    }

    TypedPolynomial<Vector3> path_;
    float age_;
};

struct Trailer: public Projectile
{
    Trailer() = default;

    Trailer(const TypedPolynomial<Vector3>& path): Projectile(path)
    {
        path_.SetSlope({ -.013f, 1.f });
    }

    Color color_;
};

namespace Dry
{
    class RaycastVehicle;
}

class Mower: public Controllable
{
    DRY_OBJECT(Mower, Controllable);

public:
    Mower(Context* context);
    void OnSetEnabled() override;
    void Start() override;
    void DelayedStart() override;
    void Stop() override;

    void Update(float timeStep) override;
    void PostUpdate(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void FixedPostUpdate(float timeStep) override;

    bool Save(Serializer& dest) const override;
    bool SaveXML(XMLElement& dest) const override;
    bool SaveJSON(JSONValue& dest) const override;
    void MarkNetworkUpdate() override;
    void GetDependencyNodes(PODVector<Node*>& dest) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

protected:
    void OnNodeSet(Node* node) override;
    void OnSceneSet(Scene* scene) override;
    void OnMarkedDirty(Node* node) override;
    void OnNodeSetEnabled(Node* node) override;

    RaycastVehicle* vehicle_;

    TypedPolynomial<Vector3> gun_;
    Vector<Projectile> bullets_;
    Vector<Projectile> sparkles_;
    Vector<Trailer> trailers_;
    HashMap<unsigned, Color> trailerColors_;

    Pair<Projectile, Projectile> missiles_;


    float bulletInterval;
    float sinceBullet;

    float missileInterval;
    float sinceMissile;

    bool lastFiredRight_;

private:
    void RenderDebug(StringHash eventType, VariantMap& eventData);
    void Sparkle(const Vector3& position, float speed = 1.f);
    void Trail(const Vector3& position);
    void HandleInput();
    void FireMissile();
};

#endif // MOWER_H
