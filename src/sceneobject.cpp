/* MultiMower
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "sceneobject.h"

SceneObject::SceneObject(Context *context): LogicComponent(context),
    randomizer_{ Random() }
{
}

void SceneObject::OnNodeSet(Node *node)
{
}

void SceneObject::Set(Vector3 position)
{
    node_->SetPosition(position);
    node_->SetEnabledRecursive(true);
}

Vector3 SceneObject::GetPosition() const
{
    return node_->GetWorldPosition();
}

void SceneObject::Disable()
{
    node_->SetEnabledRecursive(false);
}

void SceneObject::PlaySample(Sound* sample, float gain)
{
    SoundSource3D* s{ CreateSoundSource() };

    s->SetGain(gain);
    s->Play(sample);
    return;

}

SoundSource3D* SceneObject::CreateSoundSource()
{
    Node* sampleNode{ node_->GetScene()->CreateChild("Sample") };
    SoundSource3D* sampleSource{ sampleNode->CreateComponent<SoundSource3D>() };
    sampleSource->SetSoundType(SOUND_EFFECT);
    sampleSource->SetAutoRemoveMode(REMOVE_NODE);
    sampleSource->SetDistanceAttenuation(4.f, 23.f, 3.f);

    return sampleSource;
}
