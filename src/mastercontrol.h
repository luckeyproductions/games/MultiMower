/* MultiMower
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"

#define MC GetSubsystem<MasterControl>()

namespace Dry {
class Node;
class Scene;
}

class Player;
class Jib;

class MasterControl: public Application
{
    DRY_OBJECT(MasterControl, Application);
public:
    MasterControl(Context* context);

    Scene* GetScene() const { return scene_; }

    int GetPlayerId() { return ++lastId_; }

    Player* AddPlayer();
    Player* GetPlayer(int playerId) const;
    Player* GetNearestPlayer(Vector3 pos);
    Vector< SharedPtr<Player> > GetPlayers();
    void RemovePlayer(Player *player);

    // Setup before engine initialization. Modifies the engine paramaters.
    void Setup() override;
    // Setup after engine initialization.
    void Start() override;
    // Cleanup after the main loop. Called by Application.
    void Stop() override;
    void Exit();

private:
    static MasterControl* instance_;

    void CreateScene();

    int lastId_{ 0 };
    Scene* scene_;
    Vector< SharedPtr<Player> > players_;
    Vector<Jib*> jibs_;
};

#endif // MASTERCONTROL_H
