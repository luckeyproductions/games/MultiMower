/* MultiMower
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "inputmaster.h"
#include "controllable.h"
#include "player.h"

Controllable::Controllable(Context* context): SceneObject(context),
    controlled_{ false },
    move_{},
    aim_{},
    maxPitch_{ 90.0f },
    minPitch_{ 0.0f },

    actions_{},
    actionSince_{},
    actionInterval_{}
{
    for (int a{ 0 }; a < 4; ++a)
    {
        actionSince_[a] = actionInterval_[a] = 1.0f;
    }
}
void Controllable::OnNodeSet(Node *node)
{
    if (!node)
        return;

    SceneObject::OnNodeSet(node);
}
void Controllable::Update(float timeStep)
{
    for (unsigned a{ 0 }; a < actions_.size(); ++a)
    {
        if (actions_[a] || actionSince_[a] > 0.0f)
            actionSince_[a] += timeStep;
    }
}

void Controllable::SetMove(Vector3 move)
{
    if (move.Length() > 1.0f)
        move.Normalize();

    move_ = move;
}

void Controllable::SetAim(Vector3 aim)
{
    aim_ = aim.Normalized();
}

void Controllable::SetActions(std::bitset<4> actions)
{
    if (actions == actions_)
    {
        return;
    }
    else
    {
        for (unsigned i{ 0 }; i < actions.size(); ++i)
        {

            if (actions[i] != actions_[i])
            {
                actions_[i] = actions[i];

                if (actions[i] && actionSince_[i] > actionInterval_[i])
                    HandleAction(i);
            }
        }
    }
}

void Controllable::HandleAction(int actionId)
{
    actionSince_[actionId] = 0.f;
}

Player* Controllable::GetPlayer()
{
    return INPUTMASTER->GetPlayerByControllable(this);
}
