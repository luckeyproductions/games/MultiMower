/* MultiMower
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mastercontrol.h"
#include "inputmaster.h"
#include "jib.h"

#include "player.h"

Player::Player(Context* context): Object(context),
    jib_{ nullptr },
    playerId_{ context_->MC->GetPlayerId() },
    autoPilot_{ false },
    alive_{ true },
    score_{ 0 },
    multiplier_{ 1 }
{
}

void Player::Die()
{
    alive_ = false;
}
void Player::Respawn()
{
    ResetScore();
    multiplier_ = 1;
    alive_ = true;
}

void Player::SetScore(int points)
{
    score_ = points;
}
void Player::ResetScore()
{
    SetScore(0);
}

void Player::SetJib(Jib* jib)
{
    jib->SetPlayer(this);
    jib_ = jib;
}

void Player::AddScore(int points)
{
    score_ += points;
}

Controllable* Player::GetControllable()
{
    Controllable* controllable{ GetSubsystem<InputMaster>()->GetControllableByPlayer(playerId_) };

    if (controllable)
        return controllable;

    else return nullptr;
}
